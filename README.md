# Perl One Liners
### 'perl -h' Help
### '-e' It allows you to specify the Perl code to be executed right on the command line
### '-n' flag feeds the input to Perl line by line.
### '-p' flag (printing loop) processes the file line by line and prints the output.
### '-i' To replace directly in the file you can use the  flag

- Process the file line by line, and return all matches
```
perl -ne 'while(/\bc\w+/g){print "$&\n";}' yourfile.txt

```
- Process the file line by line, and return all matching lines
```
perl -ne 'print if /\bc\w+/' yourfile.txt
``` 

- Replace in the input file ALL words 'cat' for 'dog' (s/find/replace/flags)
``` 
perl -pi -e 's/cat/dog/g' yourfile.txt 
``` 

- Replace word 'cat' for 'dog' with condition line must contain 'red'
``` 
perl -pe '/red/ && s/cat/dog/g' yourfile.txt 
``` 

- Numbering lines (output to temp.txt):
``` 
perl -n -e 'print "$. $_"' yourfile.txt > temp.txt
``` 

- Remove newlines:
``` 
perl -pi -e 's/\s*$//' yourfile.txt
``` 

- Remove blanklines:
``` 
perl -ne 'print unless /^$/' yourfile.txt
``` 

- Strip whitespace from the beginning and end of each line:
``` 
perl -ple 's/^[ \t]+|[ \t]+$//g' yourfile.txt
``` 

- Convert text to UPPERCASE:
```
 perl -nle 'print uc' yourfile.txt

``` 

- Convert text to lowercase:
```
perl -nle 'print lc' uppercase.txt > yourfile.txt

``` 